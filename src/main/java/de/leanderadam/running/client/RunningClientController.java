package de.leanderadam.running.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.time.ZonedDateTime;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.leanderadam.running.client.controller.EncryptionController;
import de.leanderadam.running.client.models.AbstractSerializableModel;
import de.leanderadam.running.client.models.ApiParameter;
import de.leanderadam.running.client.models.HeapModel;
import de.leanderadam.running.client.models.HeartbeatConfiguration;
import de.leanderadam.running.client.models.HeartbeatModel;
import de.leanderadam.running.client.models.SecurityWrapperModel;
import de.leanderadam.running.client.models.StartupModel;

/**
 * Where the magic happens.<br>
 * The Running-Client sends Heartbeats to the Running-Server to monitor an
 * application.
 * <p>
 * Call start() to start the Running-Client.<br>
 * Call addSession() and removeSession() to log the sessions.
 * </p>
 * <p>
 * Safe properties in
 * <code>{PROJECT_BASE}/resources/application.properties</code>.
 * </p>
 * 
 * @author Leander Adam
 *
 */
public class RunningClientController implements Serializable {
	private static final long serialVersionUID = 5231627746500533115L;

	private Logger logger;

	private final String propertieFileName = "application.properties";
	private final String propertyName_AppKey = "ladam.running.client.appkey";
	private final String propertyName_PublicKey = "ladam.running.client.key.public";
	private final String propertyName_ServerUrl = "ladam.running.client.server.url";
	private final String propertyName_ClientUrl = "ladam.running.client.client.url";
	private final String propertyName_HeartbeatInterval = "ladam.running.client.heartbeat.interval";
	private final String propertyName_Passiv = "ladam.running.client.passiv";
	private final String propertyName_NoInfoLog = "ladam.running.client.log.info";
	private final Properties properties = new Properties();

	private boolean successfullSetup = false;

	private ZonedDateTime startupTime;
	private String appKey;
	private PublicKey publicKey;
	private String serverUrl;
	private String clientUrl;
	private HeartbeatConfiguration heartbeatConfiguration = new HeartbeatConfiguration();

	private Boolean showInfoLog = true;

	private ScheduledExecutorService heartbeatScheduler;
	private AtomicInteger sessionCount = new AtomicInteger(0);

	private final ObjectMapper objectMapper;

	public RunningClientController() {
		startupTime = ZonedDateTime.now();
		objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
	}

	public synchronized void start() {
		synchronized (this) {
			_start();
		}
	}

	public void lazySetup() {
		if (!successfullSetup) {

			if (this.showInfoLog)
				log("[RunningClient] setup properties");
			readProperties();
			setupProperties();

		}
	}

	private void _start() {

		lazySetup();
		sendStartup();
		if (!heartbeatConfiguration.getPassiv()) {
			startHearbeatScheduler();
		} else {
			if (this.showInfoLog)
				log("[RunningClient] sending no heartbeat because application is set to be passiv");
		}

	}

	private void readProperties() {
		try {
			InputStream resourceAsStream = RunningClientController.class.getClassLoader()
					.getResourceAsStream(propertieFileName);
			properties.load(resourceAsStream);
		} catch (Exception e) {
			String msg = "[RunningClient] an exception occured while read property file application.properties";
			log(e, msg, true);
			throw new RuntimeException(e);
		}
	}

	private void setupProperties() {
		try {
			appKey = properties.getProperty(propertyName_AppKey, null);
			String _publicKeyString = properties.getProperty(propertyName_PublicKey, null);
			serverUrl = properties.getProperty(propertyName_ServerUrl, null);
			assert appKey != null : String.format("property %s is null", propertyName_AppKey);
			assert publicKey != null : String.format("property %s is null", propertyName_PublicKey);
			assert serverUrl != null : String.format("property %s is null", propertyName_ServerUrl);

			if (serverUrl.endsWith("/")) {
				serverUrl.substring(0, serverUrl.length() - 2);
			}

			clientUrl = properties.getProperty(propertyName_ClientUrl, null);

			String infoLog = properties.getProperty(propertyName_NoInfoLog, "true");
			if (infoLog != null) {
				try {
					showInfoLog = Boolean.parseBoolean(infoLog);
				} catch (Exception e) {}
			}

			byte[] publicKeybytes = EncryptionController.base64ToBytes(_publicKeyString);
			publicKey = EncryptionController.readPublicKeyFromBytes(publicKeybytes);
			assert publicKey != null;

			successfullSetup = true;

			String heartbeatIntervall = properties.getProperty(propertyName_HeartbeatInterval, null);
			String isPassiv = properties.getProperty(propertyName_Passiv, null);
			if (heartbeatIntervall != null) {
				this.heartbeatConfiguration.setRefreshInterval(Integer.valueOf(heartbeatIntervall));
			}
			if (isPassiv != null) {
				this.heartbeatConfiguration.setPassiv(Boolean.valueOf(isPassiv));
			}
		} catch (Exception e) {
			String msg = "[RunningClient] an exception occured while loading properties";
			log(e, msg, true);
			throw new RuntimeException(e);
		}

	}

	public void startHearbeatScheduler() {
		if (heartbeatScheduler == null) {
			if (this.showInfoLog)
				log("[RunningClient] starting HearbeatScheduler");
			int initialDelay = 5;
			heartbeatScheduler = Executors.newSingleThreadScheduledExecutor();
			heartbeatScheduler.scheduleWithFixedDelay(this::sendHeartbeat, initialDelay,
					heartbeatConfiguration.getRefreshInterval(), TimeUnit.SECONDS);
		} else {
			if (this.showInfoLog)
				log("[RunningClient] HearbeatScheduler is already ruinning");
		}
	}

	public void stopHeartbeatScheduler() {
		if (heartbeatScheduler != null) {
			if (this.showInfoLog)
				log("[RunningClient] stopping HearbeatScheduler");
			heartbeatScheduler.shutdown();
			heartbeatScheduler = null;
		} else {
			if (this.showInfoLog)
				log("[RunningClient] HearbeatScheduler is already stopped");
		}
	}

	private void sendHeartbeat() {
		sendHeartbeat(null);
	}

	private void sendHeartbeat(String transaction) {
		if (this.showInfoLog)
			log("[RunningClient] sending heartbeat to RunningServer");
		try {

			HeartbeatModel model = createHeartbeatModel();

			int responseCode = sendModel("heartbeat", model);

			if (responseCode != 204) {
				log(String.format("[RunningClient] response for heartbeat is %d but should be 402", responseCode));
			}

		} catch (Exception e) {
			String msg = "[RunningClient] an exception occoured while sending heartbeat";
			log(e, msg, true);
		}
	}

	private HeartbeatModel createHeartbeatModel() {
		HeapModel currentHeap = null;
		try {
			currentHeap = getCurrentHeap();
		} catch (Exception e) {
			e.printStackTrace();
		}

		HeartbeatModel model = new HeartbeatModel(appKey);
		model.setHeapVM(currentHeap);
		model.setSessionCount(this.sessionCount.get());
		return model;
	}

	private void sendStartup() {
		if (this.showInfoLog)
			log("[RunningClient] sending startup to RunningServer");
		try {

			StartupModel model = new StartupModel(appKey, startupTime);
			model.setHeartbeatConfiguration(heartbeatConfiguration);
			model.setUrl(clientUrl);

			int responseCode = sendModel("startup", model);

			if (responseCode != 204) {
				log(String.format("[RunningClient] response for startup is %d but should be 402", responseCode));
			}

		} catch (Exception e) {
			String msg = "[RunningClient] an exception occoured while sending startup";
			log(e, msg, true);
		}
	}

	private int sendModel(String urlPath, AbstractSerializableModel model) throws Exception {

		String modelJson = parseObjectToJson(model);
		SecurityWrapperModel wrapper = EncryptionController.createSecurityWrapper(appKey, publicKey, modelJson);

		byte[] wrapperBytes = parseObjectToJson(wrapper).getBytes();

		URL url = new URL(String.format("%s/%s", serverUrl, urlPath));

		HttpURLConnection connection = null;

		connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty(ApiParameter.API_NAME.getKey(), ApiParameter.API_NAME.getValue());
		connection.setRequestProperty(ApiParameter.APPLICATION_KEY.getKey(), appKey);
		connection.setDoOutput(true);
		connection.getOutputStream().write(wrapperBytes);
		connection.getOutputStream().flush();
		connection.getOutputStream().close();
		return connection.getResponseCode();

	}

	public void doPost_ActiveHeartbeat(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		System.out.println("[AbstractActiveHeartbeatHttpServlet] receiving request");

		final String appKey_reg = req.getHeader(ApiParameter.APPLICATION_KEY.getKey());

		if (appKey_reg == null) {
			// 412 - Precondition Failed
			System.out.println("[AbstractActiveHeartbeatHttpServlet] active heartbeat request has missing headers");
			resp.sendError(412, "missing header");
			return;
		}

		lazySetup();

		if (!getAppKey().equals(appKey_reg)) {
			// 409 - Conflict
			System.out.println(
					"[AbstractActiveHeartbeatHttpServlet] active heartbeat request has different application key than this application");
			resp.sendError(409, "wrong app key");
			return;
		}

		try {

			// do magic, resp code is setted by controller
			sendFinal_ActiveHeartbeatRespone(resp);

		} catch (GeneralSecurityException e) {
			System.out.println("[AbstractActiveHeartbeatHttpServlet] response error \"" + e.getMessage());
			resp.sendError(500, "security");
		} catch (IOException e) {
			System.out.println("[AbstractActiveHeartbeatHttpServlet] response error \"" + e.getMessage());
			resp.sendError(500, "connection");
		} catch (Exception e) {
			System.out.println("[AbstractActiveHeartbeatHttpServlet] internal server error \"" + e.getMessage());
			// 500 - internal server error
			resp.sendError(500, "internal");
		}

	}

	@Deprecated
	public void sendFinal_ActiveHeartbeatRespone(HttpServletResponse response)
			throws GeneralSecurityException, IOException, Exception {

		final String transactionID = response.getHeader(ApiParameter.TRANSACTION.getKey());

		HeartbeatModel model = createHeartbeatModel();
		model.setTransaction(transactionID != null ? transactionID : "none in request");
		String modelJson = parseObjectToJson(model);
		SecurityWrapperModel wrapper = EncryptionController.createSecurityWrapper(appKey, publicKey, modelJson);

		byte[] wrapperBytes = parseObjectToJson(wrapper).getBytes();

		response.setHeader(ApiParameter.API_NAME.getKey(), ApiParameter.API_NAME.getValue());
		response.setHeader(ApiParameter.APPLICATION_KEY.getKey(), appKey);
		response.getOutputStream().write(wrapperBytes);
		response.getOutputStream().flush();
		response.getOutputStream().close();
		response.setStatus(200);

	}

	public <T extends Serializable> String parseObjectToJson(T object) throws JsonProcessingException {
		return objectMapper.writeValueAsString(object);
	}

	private HeapModel getCurrentHeap() {
		final Runtime runtime = Runtime.getRuntime();
		return new HeapModel(runtime.freeMemory(), runtime.totalMemory(), runtime.maxMemory());
	}

	public void log(String message) {
		log(null, Level.INFO, message, false);
	}

	public void log(Exception e, boolean showStackTrace) {
		log(e, Level.SEVERE, showStackTrace);
	}

	public void log(Exception e, Level level, boolean showStackTrace) {
		log(e, level, showStackTrace);
	}

	public void log(Exception e, String message, boolean showStackTrace) {
		log(e, Level.SEVERE, message, showStackTrace);
	}

	public void log(Exception e, Level level, String message, boolean showStackTrace) {

		String toLog = "";

		if (message != null) {
			toLog += message;
		}

		if (e != null) {
			if (message != null) {
				toLog += " : ";
			}
			toLog += e.getMessage();
		}

		if (level == null) {
			level = e == null ? Level.INFO : Level.SEVERE;
		}

		writeLog(toLog, level);
		if (e != null && showStackTrace) {
			e.printStackTrace();
		}

	}

	private void writeLog(String log, Level level) {
		if (logger != null) {
			logger.log(level, log);
		} else {
			System.out.println(level.toString() + " " + log);
		}
	}

	public Properties getProperties() {
		return properties;
	}

	public String getProperty(String name, String defaultValue) {
		return getProperties().getProperty(name == null ? "" : name, defaultValue);

	}

	public String getProperty(String name) {
		return getProperty(name, null);
	}

	public String getClientUrl() {
		return clientUrl;
	}

	public void setClientUrl(String clientUrl) {
		this.clientUrl = clientUrl;
	}

	public HeartbeatConfiguration getHeartbeatConfiguration() {
		return heartbeatConfiguration;
	}

	public void setHeartbeatConfiguration(HeartbeatConfiguration heartbeatConfiguration) {
		this.heartbeatConfiguration = heartbeatConfiguration;
	}

	public String getAppKey() {
		return appKey;
	}

	public PublicKey getPublicKey() {
		return publicKey;
	}

	public String getServerUrl() {
		return serverUrl;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public void addSession() {
		this.sessionCount.getAndIncrement();
	}

	public void removeSession() {
		this.sessionCount.getAndDecrement();
	}

}