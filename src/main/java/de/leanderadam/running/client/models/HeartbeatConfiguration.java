package de.leanderadam.running.client.models;

import java.io.Serializable;

/**
 * Configuration for sending Heartbeats.
 * 
 * @author Leander Adam
 *
 */
public class HeartbeatConfiguration implements Serializable {
	private static final long serialVersionUID = 409400785125823667L;

	private Integer refreshInterval = 60 * 15;
	private Boolean passiv = true;

	public HeartbeatConfiguration() {}

	/**
	 * 
	 * @return the interval in seconds
	 */
	public Integer getRefreshInterval() {
		return refreshInterval;
	}

	/**
	 * set the interval in seconds
	 * 
	 * @param refreshIntervall
	 */
	public void setRefreshInterval(Integer refreshIntervall) {
		this.refreshInterval = refreshIntervall;
	}

	public Boolean getPassiv() {
		return passiv;
	}

	public void setPassiv(Boolean passiv) {
		this.passiv = passiv;
	}

}
