package de.leanderadam.running.client.models;

import java.io.Serializable;

/**
 * Wrapper for encrypted communication
 * 
 * @author Leander Adam
 *
 */
public class SecurityWrapperModel implements Serializable {
	private static final long serialVersionUID = -8590880154330452294L;

	private String applicationKey;
	private String responseUrl;
	private String securityKey;
	private String encryptedContent;

	public SecurityWrapperModel() {}

	public String getApplicationKey() {
		return applicationKey;
	}

	public void setApplicationKey(String applicationKey) {
		this.applicationKey = applicationKey;
	}

	public String getResponseUrl() {
		return responseUrl;
	}

	public void setResponseUrl(String responseUrl) {
		this.responseUrl = responseUrl;
	}

	public String getSecurityKey() {
		return securityKey;
	}

	public void setSecurityKey(String pass) {
		this.securityKey = pass;
	}

	public String getEncryptedContent() {
		return encryptedContent;
	}

	public void setEncryptedContent(String encryptedContent) {
		this.encryptedContent = encryptedContent;
	}

}
