package de.leanderadam.running.client.models;

import java.io.Serializable;
import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import de.leanderadam.running.client.converter.CustomZonedDateTimeDeserializer;
import de.leanderadam.running.client.converter.CustomZonedDateTimeSerializer;

/**
 * Parent model.
 * 
 * @author Leander Adam
 *
 */
@JsonTypeInfo(use = Id.NAME, include = As.EXTERNAL_PROPERTY, property = "classtype")
@JsonSubTypes({ @Type(value = StartupModel.class, name = "configModel"),
		@Type(value = HeartbeatModel.class, name = "heartbeatModel"), })
public abstract class AbstractSerializableModel implements Serializable {
	private static final long serialVersionUID = -2711927321268039878L;

	private String applicationKey;
	@JsonSerialize(using = CustomZonedDateTimeSerializer.class)
	@JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
	private ZonedDateTime creation = ZonedDateTime.now();
	private String transaction;

	public AbstractSerializableModel() {}

	public AbstractSerializableModel(String applicationKey) {
		super();
		this.applicationKey = applicationKey;
	}

	public String getApplicationKey() {
		return applicationKey;
	}

	public ZonedDateTime getCreation() {
		return creation;
	}

	public String getTransaction() {
		return transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}

	public void setApplicationKey(String applicationKey) {
		this.applicationKey = applicationKey;
	}

	public void setCreation(ZonedDateTime creation) {
		this.creation = creation;
	}

}
