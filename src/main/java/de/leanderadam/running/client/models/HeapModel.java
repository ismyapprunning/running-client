package de.leanderadam.running.client.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZonedDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import de.leanderadam.running.client.converter.CustomZonedDateTimeDeserializer;
import de.leanderadam.running.client.converter.CustomZonedDateTimeSerializer;

/**
 * The current heap usage of an application.
 * 
 * @author Leander Adam
 *
 */
public class HeapModel implements Serializable {
	private static final long serialVersionUID = 2335561348755481910L;

	private BigDecimal freeMemory;
	private BigDecimal totalMemory;
	private BigDecimal maxMemory;
	private BigDecimal usedMemory;
	@JsonSerialize(using = CustomZonedDateTimeSerializer.class)
	@JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
	private ZonedDateTime creationDateTime;

	public HeapModel() {}

	public HeapModel(double freeMemory, double totalMemory, double maxMemory) {
		super();
		this.freeMemory = new BigDecimal(freeMemory).divide(new BigDecimal(1000000.0)).setScale(0, RoundingMode.DOWN);
		this.totalMemory = new BigDecimal(totalMemory).divide(new BigDecimal(1000000.0)).setScale(0, RoundingMode.DOWN);
		this.maxMemory = new BigDecimal(maxMemory).divide(new BigDecimal(1000000.0)).setScale(0, RoundingMode.DOWN);
		this.usedMemory = this.totalMemory.subtract(this.freeMemory).setScale(0, RoundingMode.DOWN);
		this.creationDateTime = ZonedDateTime.now();
	}

	public BigDecimal getFreeMemory() {
		return freeMemory;
	}

	public BigDecimal getTotalMemory() {
		return totalMemory;
	}

	public BigDecimal getMaxMemory() {
		return maxMemory;
	}

	public BigDecimal getUsedMemory() {
		return usedMemory;
	}

	public ZonedDateTime getCreationDateTime() {
		return creationDateTime;
	}

	public void setFreeMemory(BigDecimal freeMemory) {
		this.freeMemory = freeMemory;
	}

	public void setTotalMemory(BigDecimal totalMemory) {
		this.totalMemory = totalMemory;
	}

	public void setMaxMemory(BigDecimal maxMemory) {
		this.maxMemory = maxMemory;
	}

	public void setUsedMemory(BigDecimal usedMemory) {
		this.usedMemory = usedMemory;
	}

	public void setCreationDateTime(ZonedDateTime creationDateTime) {
		this.creationDateTime = creationDateTime;
	}

}
