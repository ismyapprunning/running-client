package de.leanderadam.running.client.models;

/**
 * Model for the current heartbeat.
 * 
 * @author Leander Adam
 *
 */
@AnnotatedModel(name = "HeartbeatModel", version = "1.0")
public class HeartbeatModel extends AbstractSerializableModel {
	private static final long serialVersionUID = 7778839613752352352L;

	private HeapModel heapModel;
	private Integer sessionCount;

	public HeartbeatModel() {}

	public HeartbeatModel(String applicationKey) {
		super(applicationKey);
	}

	public HeapModel getHeapVM() {
		return heapModel;
	}

	public void setHeapVM(HeapModel heapModel) {
		this.heapModel = heapModel;
	}

	public Integer getSessionCount() {
		return sessionCount;
	}

	public void setSessionCount(Integer sessionCount) {
		this.sessionCount = sessionCount;
	}

}
