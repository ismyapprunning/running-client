package de.leanderadam.running.client.models;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import de.leanderadam.running.client.converter.CustomZonedDateTimeDeserializer;
import de.leanderadam.running.client.converter.CustomZonedDateTimeSerializer;

/**
 * Used on Running-Client (and application) startup to send status to
 * Running-Server.
 * 
 * @author Leander Adam
 *
 */
@AnnotatedModel(name = "ConfigModel", version = "1.0")
public class StartupModel extends AbstractSerializableModel {
	private static final long serialVersionUID = 3347101940478468440L;

	@JsonSerialize(using = CustomZonedDateTimeSerializer.class)
	@JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
	private ZonedDateTime startUpTime;
	private String url;
	private HeartbeatConfiguration heartbeatConfiguration;

	public StartupModel() {}

	public StartupModel(String applicationKey, ZonedDateTime startUpTime) {
		super(applicationKey);
		this.startUpTime = startUpTime;
	}

	public ZonedDateTime getStartUpTime() {
		return startUpTime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public HeartbeatConfiguration getHeartbeatConfiguration() {
		return heartbeatConfiguration;
	}

	public void setHeartbeatConfiguration(HeartbeatConfiguration heartbeatConfiguration) {
		this.heartbeatConfiguration = heartbeatConfiguration;
	}

	public void setStartUpTime(ZonedDateTime startUpTime) {
		this.startUpTime = startUpTime;
	}

}
