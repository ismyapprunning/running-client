package de.leanderadam.running.client.models;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Internal annotation for version.
 * 
 * @author Leander Adam
 *
 */
@Retention(CLASS)
@Target(TYPE)
public @interface AnnotatedModel {

	String name();

	String version();

}
