package de.leanderadam.running.client.models;

import javax.servlet.http.HttpServletRequest;

/**
 * Parameter enum for keys of api request or response headers.
 * 
 * @author Leander Adam
 *
 */
public enum ApiParameter {

	API_NAME("api-name", "IsMyAppRunning"), MODEL_TYPE("api-model-type", "undefined"),
	APPLICATION_KEY("api-app-key", "undefined"), TRANSACTION("transaction-key", "undefined");

	private final String key;
	private final String value;

	private ApiParameter(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	public static boolean doesHeaderContain(ApiParameter apiParameter, HttpServletRequest request) {
		return apiParameter.getValue().equals(request.getHeader(apiParameter.getKey()));
	}

}
