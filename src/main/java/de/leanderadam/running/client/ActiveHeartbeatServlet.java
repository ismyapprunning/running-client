package de.leanderadam.running.client;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.leanderadam.running.client.models.ApiParameter;

/**
 * Running-Server can request an active heartbeat from this client.
 * 
 * @deprecated throws NullPointerException
 * 
 * @author Leander Adam
 *
 */
//@WebServlet("/running/api/active")
@Deprecated
public abstract class ActiveHeartbeatServlet extends HttpServlet {
	private static final long serialVersionUID = 8352516216090540098L;

	public abstract RunningClientController getController();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		System.out.println("[AbstractActiveHeartbeatHttpServlet] receiving request");

		getController().lazySetup();

		final String appKey_reg = req.getHeader(ApiParameter.APPLICATION_KEY.getKey());

		if (appKey_reg == null || !getController().getAppKey().equals(appKey_reg)) {
			// 412 - Precondition Failed
			System.out.println("[AbstractActiveHeartbeatHttpServlet] active heartbeat request has missing headers");
			resp.sendError(412, "missing header");
			return;
		}

//		RunningClientController controller = getController();
		final String appKey_this = getController().getAppKey();

		if (!appKey_this.equals(appKey_reg)) {
			// 409 - Conflict
			System.out.println(
					"[AbstractActiveHeartbeatHttpServlet] active heartbeat request has different application key than this application");
			resp.sendError(409, "wrong app key");
			return;
		}

		try {

			// do magic, resp code is setted by controller
			getController().sendFinal_ActiveHeartbeatRespone(resp);

		} catch (GeneralSecurityException e) {
			System.out.println("[AbstractActiveHeartbeatHttpServlet] response error \"" + e.getMessage());
			resp.sendError(500, "security");
		} catch (IOException e) {
			System.out.println("[AbstractActiveHeartbeatHttpServlet] response error \"" + e.getMessage());
			resp.sendError(500, "connection");
		} catch (Exception e) {
			System.out.println("[AbstractActiveHeartbeatHttpServlet] internal server error \"" + e.getMessage());
			// 500 - internal server error
			resp.sendError(500, "internal");
		}

	}

}
