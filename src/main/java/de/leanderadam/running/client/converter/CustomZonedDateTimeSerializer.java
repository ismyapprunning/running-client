package de.leanderadam.running.client.converter;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * 
 * @author Leander Adam
 *
 */
public class CustomZonedDateTimeSerializer extends StdSerializer<ZonedDateTime> {
	private static final long serialVersionUID = -437436173799821291L;

	public CustomZonedDateTimeSerializer() {
		this(null);
	}

	public CustomZonedDateTimeSerializer(Class<ZonedDateTime> t) {
		super(t);
	}

	@Override
	public void serialize(ZonedDateTime value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeStartObject();
		gen.writeStringField("value", value.format(DateTimeFormatter.ISO_ZONED_DATE_TIME));
		gen.writeEndObject();
	}

}
