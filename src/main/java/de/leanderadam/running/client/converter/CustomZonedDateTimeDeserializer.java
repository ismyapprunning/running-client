package de.leanderadam.running.client.converter;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * 
 * @author Leander Adam
 *
 */
public class CustomZonedDateTimeDeserializer extends StdDeserializer<ZonedDateTime> {
	private static final long serialVersionUID = 6468535108987215437L;

	public CustomZonedDateTimeDeserializer() {
		this(null);
	}

	public CustomZonedDateTimeDeserializer(Class<?> vc) {
		super(vc);
	}

	@Override
	public ZonedDateTime deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		JsonNode node = p.getCodec().readTree(p);
		String StringValue = node.get("value").asText();
		return ZonedDateTime.parse(StringValue, DateTimeFormatter.ISO_ZONED_DATE_TIME);
	}

}
