package de.leanderadam.running.client.controller;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.EncodedKeySpec;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import de.leanderadam.running.client.models.SecurityWrapperModel;

/**
 * Does some encryption, decoding and generates {@link KeyPair}s.
 * 
 * @author Leander Adam
 *
 */
public class EncryptionController {

	public static KeyPair createKeyPair() throws NoSuchAlgorithmException {
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
		generator.initialize(2048);
		return generator.generateKeyPair();
	}

	public static byte[] writeKeyAsBytes(Key key) {
		return key.getEncoded();
	}

	public static PublicKey readPublicKeyFromBytes(byte[] bytes) throws GeneralSecurityException {
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(bytes);
		return keyFactory.generatePublic(publicKeySpec);
	}

	public static PrivateKey readPrivateKeyFromString(String string) throws GeneralSecurityException {
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		EncodedKeySpec privateKeySpec = new X509EncodedKeySpec(string.getBytes());
		return keyFactory.generatePrivate(privateKeySpec);
	}

	public static String encryptSecurityKey(PublicKey publicKey, String json) throws GeneralSecurityException {
		return bytesToBase64(encrypt(publicKey, json));
	}

	public static String decryptSecurityKey(PrivateKey privateKey, String base64) throws GeneralSecurityException {
		return decrypt(privateKey, base64ToBytes(base64));
	}

	public static byte[] encrypt(PublicKey publicKey, String content) throws GeneralSecurityException {
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		byte[] secretMessageBytes = content.getBytes(StandardCharsets.UTF_8);
		return cipher.doFinal(secretMessageBytes);
	}

	public static String decrypt(PrivateKey privateKey, byte[] bytes) throws GeneralSecurityException {
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		byte[] decyptedBytes = cipher.doFinal(bytes);
		return new String(decyptedBytes, StandardCharsets.UTF_8);
	}

	public static String bytesToBase64(byte[] bytes) {
		return Base64.getEncoder().encodeToString(bytes);
	}

	public static byte[] base64ToBytes(String content) {
		return Base64.getDecoder().decode(content);
	}

	public static SecurityWrapperModel createSecurityWrapper(String applicationKey, PublicKey publicKey, String json)
			throws GeneralSecurityException, Exception {

		String key = generateSecureRandomPassword();
		String encryptSecurityKey = encryptSecurityKey(publicKey, key);
		String encryptedJson = encrypt(json, key, key);

		SecurityWrapperModel wrapper = new SecurityWrapperModel();
		wrapper.setSecurityKey(encryptSecurityKey);
		wrapper.setEncryptedContent(encryptedJson);
		wrapper.setApplicationKey(applicationKey);

		return wrapper;
	}

	public static String getFromSecurityWrapper(PrivateKey privateKey, SecurityWrapperModel wrapper)
			throws GeneralSecurityException, Exception {
		String plainSecurityKey = decryptSecurityKey(privateKey, wrapper.getSecurityKey());
		return decrypt(wrapper.getEncryptedContent(), plainSecurityKey, plainSecurityKey);
	}

	public static String encrypt(String strToEncrypt, String key, String salt)
			throws GeneralSecurityException, Exception {
		try {
			byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			IvParameterSpec ivspec = new IvParameterSpec(iv);

			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			KeySpec spec = new PBEKeySpec(key.toCharArray(), salt.getBytes(), 65536, 256);
			SecretKey tmp = factory.generateSecret(spec);
			SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

			cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec);
			return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
		} catch (GeneralSecurityException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}

	}

	public static String decrypt(String strToDecrypt, String key, String salt)
			throws BadPaddingException, GeneralSecurityException {
		try {
			byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			IvParameterSpec ivspec = new IvParameterSpec(iv);

			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			KeySpec spec = new PBEKeySpec(key.toCharArray(), salt.getBytes(), 65536, 256);
			SecretKey tmp = factory.generateSecret(spec);
			SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, secretKey, ivspec);
			return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)), Charset.forName("UTF-8"));
		} catch (BadPaddingException e) {
			throw e;
		} catch (GeneralSecurityException e) {
			throw e;
		}
	}

	public static String generateSecureRandomPassword() {
		Stream<Character> pwdStream = Stream.concat(getRandomNumbers(3), Stream.concat(getRandomSpecialChars(3),
				Stream.concat(getRandomAlphabets(3, true), getRandomAlphabets(4, false))));
		List<Character> charList = pwdStream.collect(Collectors.toList());
		Collections.shuffle(charList);
		String password = charList.stream().collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
				.toString();
		return password;
	}

	private final static Random random = new SecureRandom();

	protected static Stream<Character> getRandomAlphabets(int count, boolean upperCase) {
		IntStream characters = null;
		if (upperCase) {
			characters = random.ints(count, 65, 90);
		} else {
			characters = random.ints(count, 97, 122);
		}
		return characters.mapToObj(data -> (char) data);
	}

	protected static Stream<Character> getRandomNumbers(int count) {
		IntStream numbers = random.ints(count, 48, 57);
		return numbers.mapToObj(data -> (char) data);
	}

	protected static Stream<Character> getRandomSpecialChars(int count) {
		IntStream specialChars = random.ints(count, 33, 45);
		return specialChars.mapToObj(data -> (char) data);
	}

}
